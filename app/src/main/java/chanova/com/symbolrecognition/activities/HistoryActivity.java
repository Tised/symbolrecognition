package chanova.com.symbolrecognition.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.sql.SQLException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.adapters.HistoryAdapter;
import chanova.com.symbolrecognition.dao.RecognitionItemDAO;
import chanova.com.symbolrecognition.interfaces.ToolbarIteractionInterface;
import chanova.com.symbolrecognition.models.ActivityResultBus;
import chanova.com.symbolrecognition.models.ActivityResultEvent;
import chanova.com.symbolrecognition.models.RecognitionItemModel;
import chanova.com.symbolrecognition.utils.DBUtils.HelperFactory;
import chanova.com.symbolrecognition.utils.DividerItemDecoration;

public class HistoryActivity extends AppCompatActivity implements ToolbarIteractionInterface {

    private static final String TAG = "HISTORY";
    private static final int ADD_NEW_ITEM_CODE = 2;
    private static final int UPDATE_ITEM_CODE = 3;
    @Bind(R.id.history_recycler)
    RecyclerView historyRecycler;
    @Bind(R.id.empty_hint)
    TextView emptyHint;

    HistoryAdapter historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //init block
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        historyAdapter = new HistoryAdapter(this);

        List<RecognitionItemModel> recognitionItemModels;

        try {
            RecognitionItemDAO recognitionItemDAO = HelperFactory.getHelper().getRecognitionItemDAO();
            recognitionItemModels = recognitionItemDAO.getAllItems();

            if (recognitionItemModels!=null)
                historyAdapter.addItems(recognitionItemModels);

            if (historyAdapter.getItemCount() == 0)
                emptyHint.setVisibility(View.VISIBLE);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //set values block
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.history));

        historyRecycler.setLayoutManager(new LinearLayoutManager(this));
        historyRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        historyRecycler.setAdapter(historyAdapter);
    }

    @Override
    public void setTitle(String string) {

        getSupportActionBar().setTitle(string);
    }

    @OnClick(R.id.fab)
    public void addNewRecognitionItem(View view) {
        startActivityForResult(new Intent(this, AddNewItemActivity.class), ADD_NEW_ITEM_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "received");

        ActivityResultBus.getInstance().postQueue(
                new ActivityResultEvent(requestCode, resultCode, data));

        if (resultCode == RESULT_OK) {

            if (requestCode == ADD_NEW_ITEM_CODE) {
                Bundle extras = data.getExtras();
                RecognitionItemModel recognitionItemModel = extras.getParcelable("item");
                emptyHint.setVisibility(View.GONE);

                historyAdapter.addItem(recognitionItemModel);
            }

            if (requestCode == UPDATE_ITEM_CODE) {

                Bundle extras = data.getExtras();

                RecognitionItemModel recognitionItemModel = extras.getParcelable("item");
                int pos = extras.getInt("pos");

                historyAdapter.updateItem(pos, recognitionItemModel);
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback);
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
}
