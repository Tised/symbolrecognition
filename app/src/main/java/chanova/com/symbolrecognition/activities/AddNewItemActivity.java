package chanova.com.symbolrecognition.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.interfaces.RecognizeItemIteractionInterface;
import chanova.com.symbolrecognition.models.RecognitionItemModel;

public class AddNewItemActivity extends AppCompatActivity implements RecognizeItemIteractionInterface {

    private static final String TAG = "ADD NEW";
    RecognitionItemModel recognitionItemModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        if (extras!=null) {

            int pos = extras.getInt("pos");
            recognitionItemModel = extras.getParcelable("item");
            Log.d(TAG, "received pos = " + pos + "item = " + recognitionItemModel);
        } else {

            recognitionItemModel = new RecognitionItemModel();
        }

        setContentView(R.layout.activity_add_new_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
    }

    @Override
    public void setPicPath(String path) {
        recognitionItemModel.setPicPath(path);
    }

    @Override
    public void setRecognizedText(String text) {
        recognitionItemModel.setText(text);
    }

    @Override
    public void setName(String name) {
        recognitionItemModel.setName(name);
    }

    @Override
    public void setMakeDate(String makeDate) {
        recognitionItemModel.setMakeDate(makeDate);
    }

    @Override
    public RecognitionItemModel getResultModel() {
        return recognitionItemModel;
    }
}
