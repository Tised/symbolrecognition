package chanova.com.symbolrecognition.utils;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DateConverterUtils {

    public static String getTimeFromDate(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        return sdf.format(date);
    }

    public static String getDayMonthFromDate(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");

        return sdf.format(date);
    }

    public static String formatDate(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_TYPE);

        return sdf.format(date);
    }
}
