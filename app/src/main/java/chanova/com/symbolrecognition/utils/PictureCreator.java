package chanova.com.symbolrecognition.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

@SuppressWarnings("deprecation")
public class PictureCreator {

	Context context;


	public PictureCreator(Context c){

		this.context = c;
	}
	
	public static Bitmap rotateBitmap(Bitmap b, int angle){
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		Bitmap rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
		//rotated = createSquaredBitmap(rotated);
		return rotated;
	  }


	public static Bitmap decodeSampledBitmapFromByteArray(byte[] picData, int reqWidth, int reqHeight)
	{ // BEST QUALITY MATCH

	//First decode with inJustDecodeBounds=true to check dimensions
	final BitmapFactory.Options options = new BitmapFactory.Options();

	options.inJustDecodeBounds = true;
	BitmapFactory.decodeByteArray(picData, 0, picData.length, options);

	// Calculate inSampleSize, Raw height and width of image
	final int height = options.outHeight;
	final int width = options.outWidth;
	options.inPreferredConfig = Config.RGB_565;
	int inSampleSize = 1;

	if (height > reqHeight)
	{
	    inSampleSize = Math.round((float) height / (float) reqHeight);
	}
	int expectedWidth = width / inSampleSize;

	if (expectedWidth > reqWidth)
	{
	    //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
	    inSampleSize = Math.round((float) width / (float) reqWidth);
	}

	options.inSampleSize = inSampleSize;

	// Decode bitmap with inSampleSize set
	options.inJustDecodeBounds = false;

	Bitmap scaled = BitmapFactory.decodeByteArray(picData, 0, picData.length, options);

	//return createSquaredBitmap(scaled);
	return scaled;
	}

	public static Bitmap drawableToBitmap (Drawable drawable) {
	    if (drawable instanceof BitmapDrawable) {
	        return ((BitmapDrawable)drawable).getBitmap();
	    }

	    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap);
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);

	    return bitmap;
	}
	
}
