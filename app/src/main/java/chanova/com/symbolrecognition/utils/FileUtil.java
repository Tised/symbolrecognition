package chanova.com.symbolrecognition.utils;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;


public class FileUtil {

    public static void writeToFile(Bitmap bm, String fileName){

        try
        {
            File pic = new File(fileName);
            FileOutputStream os = new FileOutputStream(pic);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            os = null;
        }
        catch (Exception e)
        {
        }
    }

    public static void deleteFile(String path){

        try
        {
            File pic = new File(path);
            pic.delete();
        }
        catch (Exception e)
        {
        }
    }
}
