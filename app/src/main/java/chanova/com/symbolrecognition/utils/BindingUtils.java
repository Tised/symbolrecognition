package chanova.com.symbolrecognition.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;


public class BindingUtils {

    @BindingAdapter({"bind:pic"})
    public static void setPlaying(ImageView view, String path){

        if (path != null)
            Picasso.with(view.getContext()).load(new File(path)).into(view);
    }

    @BindingAdapter({"bind:thub"})
    public static void resizeToThub(ImageView view, String path){

        if (path != null)
            Picasso.with(view.getContext())
                    .load(new File(path))
                    .resize(48, 48)
                    .centerCrop()
                    .into(view);
    }


}
