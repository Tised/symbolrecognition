package chanova.com.symbolrecognition.custom_views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import chanova.com.symbolrecognition.SymbolRecognition;
import chanova.com.symbolrecognition.utils.DimensUtil;
import chanova.com.symbolrecognition.utils.FileUtil;


public class MySurfaceView extends SurfaceView implements Runnable{

    private static final String TAG = "LAYOUT VIEW";
    String path;
    Bitmap pic;
    PointF leftTopCorner, rightTopCorner, leftBottomCorner, rightBottomCorner, activeAngle;
    Canvas canvas;

    ArrayList<PointF> corners;

    public MySurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    Thread thread = null;
    SurfaceHolder surfaceHolder;
    volatile boolean running = false;

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Random random;

    volatile boolean touched = false;
    volatile float touched_x, touched_y;



    public MySurfaceView(Context context, Bitmap path) {
        super(context);

        surfaceHolder = getHolder();
        random = new Random();

        pic = path;
        initPoints();
    }

    public void onResumeMySurfaceView(){
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public void onPauseMySurfaceView(){
        boolean retry = true;
        running = false;
        while(retry){
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {

        while(running){
            if(surfaceHolder.getSurface().isValid()){

                drawPreview();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        touched_x = event.getX();
        touched_y = event.getY();

        int action = event.getAction();

        switch(action){
            case MotionEvent.ACTION_DOWN:

                activeAngle = getActiveAngle(event);
                touched = true;
                break;
            case MotionEvent.ACTION_MOVE:

                activeAngle.x = touched_x;
                activeAngle.y = touched_y;

                if (touched_x < 0)
                    activeAngle.x = 0;
                if (touched_y < 0)
                    activeAngle.y = 0;

                if (touched_y > surfaceHolder.getSurfaceFrame().height())
                    activeAngle.y = surfaceHolder.getSurfaceFrame().height();

                touched = true;
                break;
            case MotionEvent.ACTION_UP:
                activeAngle = null;
                touched = false;
                break;
            case MotionEvent.ACTION_CANCEL:
                touched = false;
                break;
            case MotionEvent.ACTION_OUTSIDE:
                touched = false;
                break;
            default:
        }
        return true; //processed
    }

    private PointF getActiveAngle(MotionEvent event){

        float[] points = new float[4];

        float min = 0;
        int minIndex = 0;

        for (int i = 0; i < 4; i++){

            points[i] = (float) (Math.pow(corners.get(i).x - event.getX(), 2) + Math.pow(corners.get(i).y - event.getY(), 2));

            if (i == 0)
                min = points[i];

            if (i > 0 && min > points[i]){
                min = points[i];
                minIndex = i;
            }
        }

        return corners.get(minIndex);
    }

    private void drawPreview() {

        canvas = surfaceHolder.lockCanvas();

        canvas.drawBitmap(pic, 0.0f, 0.0f, new Paint());

        //selection area
        Path outerPath = new Path();
        // add rect covering the whole view area
        outerPath.addRect(0, 0, surfaceHolder.getSurfaceFrame().width(), surfaceHolder.getSurfaceFrame().height(), Path.Direction.CW);

        outerPath.addPath(selectionAreaPath());

        // set the fill rule so inner area will not be painted
        outerPath.setFillType(Path.FillType.EVEN_ODD);

        // set up paints
        Paint outerPaint = new Paint();
        outerPaint.setColor(0x60000000);

        Paint borderPaint = new Paint();
        borderPaint.setARGB(255, 255, 128, 0);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(4);

        Paint transparentFill = new Paint();
        transparentFill.setColor(Color.parseColor("#00000000"));
        transparentFill.setStyle(Paint.Style.FILL);

        Paint circles= new Paint();
        circles.setARGB(255, 255, 128, 0);
        circles.setStyle(Paint.Style.FILL);

        int circleRadius = 12;

        // draw
        canvas.drawPath(outerPath, outerPaint);
        canvas.drawPath(selectionAreaPath(), borderPaint);
        canvas.drawPath(selectionAreaPath(), transparentFill);
        canvas.drawCircle(leftTopCorner.x, leftTopCorner.y, circleRadius, circles);
        canvas.drawCircle(leftBottomCorner.x, leftBottomCorner.y, circleRadius, circles);
        canvas.drawCircle(rightBottomCorner.x, rightBottomCorner.y, circleRadius, circles);
        canvas.drawCircle(rightTopCorner.x, rightTopCorner.y, circleRadius, circles);
        canvas.drawARGB(0, 0,0,0);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    private Path selectionAreaPath(){

        Path path = new Path();

        path.moveTo(leftTopCorner.x, leftTopCorner.y);
        path.lineTo(rightTopCorner.x, rightTopCorner.y);
        path.lineTo(rightBottomCorner.x, rightBottomCorner.y);
        path.lineTo(leftBottomCorner.x, leftBottomCorner.y);
        path.lineTo(leftTopCorner.x, leftTopCorner.y);

        return path;
    }

    private void initPoints(){

        float x = 50;
        float y = 30;
        float bottom = 200;
        float step = 20;

        corners = new ArrayList<>();

        leftTopCorner = new PointF();
        leftTopCorner.set(x, y);

        rightTopCorner = new PointF();
        rightTopCorner.set(DimensUtil.getScreenWidth(getContext()) - x, y);

        rightBottomCorner = new PointF();
        rightBottomCorner.set(DimensUtil.getScreenWidth(getContext()) - x + step, DimensUtil.getScreenHeight(getContext()) - bottom);

        leftBottomCorner = new PointF();
        leftBottomCorner.set(x - step, DimensUtil.getScreenHeight(getContext()) - bottom);

        corners.add(leftTopCorner);
        corners.add(rightTopCorner);
        corners.add(rightBottomCorner);
        corners.add(leftBottomCorner);
    }

    public String getResultPic(){

        Bitmap mutableBitmap = pic.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(mutableBitmap);
        canvas.drawBitmap(mutableBitmap, 0.0f, 0.0f, new Paint());

        //selection area
        Path outerPath = new Path();
        Path subPath = selectionAreaPath();
        Matrix scaleMatrix = new Matrix();
        RectF rectF = new RectF();
        subPath.computeBounds(rectF, true);
        scaleMatrix.setScale(1.0f, 1.0f, rectF.centerX(), rectF.centerY());
        subPath.transform(scaleMatrix);

        // add rect covering the whole view area
        outerPath.addRect(0, 0, mutableBitmap.getWidth(), mutableBitmap.getHeight(), Path.Direction.CW);

        outerPath.addPath(subPath);

        // set the fill rule so inner area will not be painted
        outerPath.setFillType(Path.FillType.EVEN_ODD);

        // set up paints
        Paint outerPaint = new Paint();
        outerPaint.setColor(0xFFFFFFFF);

        Paint borderPaint = new Paint();
        borderPaint.setARGB(255, 255, 128, 0);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(4);

        Paint transparentFill = new Paint();
        transparentFill.setColor(Color.parseColor("#00ffffff"));
        transparentFill.setStyle(Paint.Style.FILL);

        // draw
        canvas.drawPath(outerPath, outerPaint);
        canvas.drawPath(subPath, transparentFill);

        String path = SymbolRecognition.DATA_PATH + File.separator
                + System.currentTimeMillis() + "_cropped_result" + ".jpg";

        FileUtil.writeToFile(mutableBitmap, path);

        return path;
    }
}
