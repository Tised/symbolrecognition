package chanova.com.symbolrecognition;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import java.io.File;

import chanova.com.symbolrecognition.utils.DBUtils.HelperFactory;


public class SymbolRecognition extends Application {

    private static final String APP_TAG = "APP TAG";
    private static final String TAG = "APP";
    public static String DATA_PATH = Environment.getExternalStorageDirectory()+ File.separator + "SymbolRecognition";

    // You should have the trained data file in assets folder
    // You can get them at:
    // http://code.google.com/p/tesseract-ocr/downloads/list
    public static final String lang = "eng";
    @Override
    public void onCreate(){
        super.onCreate();

        Log.d(APP_TAG, "app started");
        HelperFactory.setHelper(getApplicationContext());

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            //handle case of no SDCARD present
        } else {
            //creating app folder at SD card
            String dir = DATA_PATH;
            //create folder
            File folder = new File(dir); //folder name
            if (!folder.exists())
                folder.mkdirs();
        }
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
