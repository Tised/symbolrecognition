package chanova.com.symbolrecognition.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.SymbolRecognition;
import chanova.com.symbolrecognition.asynctasks.DetectTextTask;
import chanova.com.symbolrecognition.interfaces.RecognizeItemIteractionInterface;
import chanova.com.symbolrecognition.interfaces.ResultInterface;
import chanova.com.symbolrecognition.models.RecognitionItemModel;
import chanova.com.symbolrecognition.utils.DBUtils.HelperFactory;
import chanova.com.symbolrecognition.utils.DateConverterUtils;


public class ResultFragment extends Fragment implements ResultInterface {

    private static final String CROPPED_PIC = "param2";
    private static final String TAG = "RESULT PIC";
    private static String ORIG_PATH = "ORIG PATH";
    private String cropPicPath;
    ImageView next;

    @Bind(R.id.result_text)
    EditText result;

    private java.lang.String lang = "eng";
    private RecognizeItemIteractionInterface mListener;
    private String originalPath;

    Bitmap original;

    @Bind(R.id.imageView2)
    ImageView processPic;


    public static ResultFragment newInstance(String cropPath, String originalPath) {

        ResultFragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putString(CROPPED_PIC, cropPath);
        args.putString(ORIG_PATH, originalPath);
        fragment.setArguments(args);
        return fragment;
    }

    public ResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            cropPicPath = getArguments().getString(CROPPED_PIC);
            originalPath = getArguments().getString(ORIG_PATH);
            mListener.setPicPath(cropPicPath);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_result, container, false);

        ButterKnife.bind(this, rootView);

        original = BitmapFactory.decodeFile(cropPicPath);

        next = (ImageView) rootView.findViewById(R.id.next);
//
        if (dataAvailable())
            detectText();

        processPic.setImageBitmap(original);

        return rootView;
    }

    String[] files = {"eng","deu","letsgodigital","lato","lato_1","archivo","blogger"};

    private boolean dataAvailable() {

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            //handle case of no SDCARD present
        } else {
            //creating app folder at SD card
            String dir = SymbolRecognition.DATA_PATH + "/tessdata/";
            //create folder
            File folder = new File(dir); //folder name
            if (!folder.exists())
                folder.mkdirs();

            AssetManager assetManager = getActivity().getAssets();

            for (int i = 0; i < files.length; i++) {

                InputStream is = null;

                try {
                    is = assetManager.open(files[i] + ".traineddata");


                File out = new File(dir, files[i] + ".traineddata");
                    if (out.exists())
                        continue;

                byte[] buffer = new byte[1024];
                FileOutputStream fos = new FileOutputStream(out);
                int read = 0;

                while ((read = is.read(buffer, 0, 1024)) >= 0) {
                    fos.write(buffer, 0, read);
                }

                fos.flush();
                fos.close();
                is.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            detectText();
        }

        return false;
    }

    private void detectText() {
        new DetectTextTask(ResultFragment.this, getActivity(), cropPicPath).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RecognizeItemIteractionInterface) {
            mListener = (RecognizeItemIteractionInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement RecognizeItemIteractionInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    @Override
    public void processResultText(String text) {

        result.setText(text);
        next.setOnClickListener(view -> {

            mListener.setMakeDate(DateConverterUtils.formatDate(new Date()));
            mListener.setRecognizedText(result.getText().toString());

            RecognitionItemModel recognitionItemModel = mListener.getResultModel();

            Intent intent = new Intent();
            intent.putExtra("item", recognitionItemModel);
            getActivity().setResult(Activity.RESULT_OK, intent);

            try {

                Log.d(TAG, "adding new bundle item");
                HelperFactory.getHelper().getRecognitionItemDAO().create(recognitionItemModel);
//                FileUtil.deleteFile(originalPath);
                getActivity().finish();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void setPic(Bitmap result) {

        processPic.setImageBitmap(result);
    }
}
