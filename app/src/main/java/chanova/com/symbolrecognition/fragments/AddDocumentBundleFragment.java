package chanova.com.symbolrecognition.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.andreabaccega.widget.FormEditText;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.SymbolRecognition;
import chanova.com.symbolrecognition.databinding.FragmentAddRecognitionPicBinding;
import chanova.com.symbolrecognition.interfaces.RecognizeItemIteractionInterface;
import chanova.com.symbolrecognition.models.ActivityResultBus;
import chanova.com.symbolrecognition.models.ActivityResultEvent;
import chanova.com.symbolrecognition.utils.FragmentUtils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class AddDocumentBundleFragment extends Fragment {

    private static final String TAG = "ADD BUNDLE";
    private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 3;
    TextView readyBundle;
    FormEditText bundleName;
    private RecognizeItemIteractionInterface mListener;

    public static AddDocumentBundleFragment newInstance() {

        AddDocumentBundleFragment fragment = new AddDocumentBundleFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    public AddDocumentBundleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentAddRecognitionPicBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_recognition_pic,
                container, false);

        binding.setBundle(mListener.getResultModel());

        View rootView = binding.getRoot();
        ButterKnife.bind(this, rootView);

        readyBundle = (TextView) rootView.findViewById(R.id.ready_bundle);
        bundleName = (FormEditText) rootView.findViewById(R.id.bundle_name_layout);

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @OnClick(R.id.add_photo)
    public void addPhotoClick(View view) {

        FormEditText[] allFields    = { bundleName };


        boolean allValid = true;
        for (FormEditText field: allFields) {
            allValid = field.testValidity() && allValid;
        }

        if (allValid) {
            // YAY

            hideSoftKeyboard(getActivity());

            mListener.setName(bundleName.getText().toString());

            //startCameraActivity();
//            AddDocumentBundleFragmentPermissionsDispatcher.openImageIntentWithCheck(this);
            openImageIntent();
        } else {
            // EditText are going to appear with an exclamation mark and an explicative message.
        }

    }

    String tempPath = SymbolRecognition.DATA_PATH + "/temp.jpg";
    protected void startCameraActivity() {
        File file = new File(tempPath);
        Uri outputFileUri = Uri.fromFile(file);

        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(intent, 0);
    }

    private Uri outputFileUri;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        AddDocumentBundleFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.MANAGE_DOCUMENTS})
    public void openImageIntent() {

        final File sdImageMainDirectory = new File(tempPath);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getActivity().getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (resultCode == -1) {
//
//            openPreview(tempPath);
//        } else {
//            Log.v(TAG, "User cancelled");
//        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == YOUR_SELECT_PICTURE_REQUEST_CODE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;
                if (isCamera) {
                    openPreview(tempPath);
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                    openPreview(getRealPathFromUri(getContext(), selectedImageUri));
                }
            }
        }
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void openPreview(String absPath) {

        Bundle bundle = new Bundle();

        // bundle.putSerializable("photoset", p);
        bundle.putString("file", absPath);

        PictureCutFragment pictureCutFragment = PictureCutFragment.newInstance(absPath);

        FragmentUtils.openFragment(pictureCutFragment, R.id.root_frame, "CUT", getActivity(), true);
    }


    @OnClick(R.id.ready_bundle)
    public void readyBundleClick(View view) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RecognizeItemIteractionInterface) {
            mListener = (RecognizeItemIteractionInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement RecognizeItemIteractionInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}
