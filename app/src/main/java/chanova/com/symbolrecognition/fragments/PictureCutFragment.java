package chanova.com.symbolrecognition.fragments;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;

import java.io.File;

import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.SymbolRecognition;
import chanova.com.symbolrecognition.utils.FragmentUtils;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class PictureCutFragment extends Fragment {

    private static final String PIC_PATH = "param1";
    private static final String TAG = "CUT FRAGMENT";
    private String picPath;
    private String cropped;
    ImageView next;
    ImageView pic;
    FrameLayout frameLayout;
    private CropImageView mCropView;
    SweetAlertDialog progress;

    public static PictureCutFragment newInstance(String param1) {

        PictureCutFragment fragment = new PictureCutFragment();
        Bundle args = new Bundle();
        args.putString(PIC_PATH, param1);

        fragment.setArguments(args);
        return fragment;
    }

    public PictureCutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            picPath = getArguments().getString(PIC_PATH);
        }

        progress = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        progress.setTitleText(getString(R.string.cropping));
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_picture_cut, container, false);

        frameLayout = (FrameLayout) rootView.findViewById(R.id.pic_layout);

        mCropView = (CropImageView) rootView.findViewById(R.id.cropImageView);
        mCropView.setCropMode(CropImageView.CropMode.FREE);
        mCropView.setMinFrameSizeInDp(10);
        mCropView.setCompressQuality(100);

        next = (ImageView) rootView.findViewById(R.id.next);

        next.setOnClickListener(view -> {

            progress.show();

            cropped = SymbolRecognition.DATA_PATH + File.separator
                    + System.currentTimeMillis() + "_cropped_result" + ".png";

            mCropView.startCrop(

                    Uri.fromFile(new File(cropped)),

                    new CropCallback() {
                        @Override
                        public void onSuccess(Bitmap cropped) {

                            progress.setTitleText(getString(R.string.saving));
                        }

                        @Override
                        public void onError() {}
                    },

                    new SaveCallback() {
                        @Override
                        public void onSuccess(Uri outputUri) {

                            progress.dismiss();

                            ResultFragment resultFragment = ResultFragment
                                    .newInstance(PictureCutFragment.this.cropped, picPath);

                            FragmentUtils.openFragment(resultFragment, R.id.root_frame, "RESULT", getContext(), true);
                        }

                        @Override
                        public void onError() {}
                    }
            );


        });

        return rootView;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        mCropView.startLoad(

                Uri.fromFile(new File(picPath)),

                new LoadCallback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
