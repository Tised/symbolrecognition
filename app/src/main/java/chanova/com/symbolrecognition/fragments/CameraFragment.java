package chanova.com.symbolrecognition.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.SymbolRecognition;
import chanova.com.symbolrecognition.models.ActivityResultBus;
import chanova.com.symbolrecognition.models.ActivityResultEvent;
import chanova.com.symbolrecognition.utils.FragmentUtils;
import chanova.com.symbolrecognition.utils.PictureCreator;

public class CameraFragment extends Fragment implements SurfaceHolder.Callback, Camera.ShutterCallback,
        Camera.AutoFocusCallback, Camera.PictureCallback {

    private static final String TAG = "CAMERA";
    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private SurfaceView preview, transparentView;
    ProgressDialog progressDialog;
    private static final int SELECT_FILE = 2222;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  preview.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.activity_camera, container, false);

        ButterKnife.bind(this, rootView);

        // наше SurfaceView имеет имя SurfaceView01
        preview = (SurfaceView) rootView.findViewById(R.id.camera_picture);

        surfaceHolder = preview.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        transparentView = (SurfaceView) rootView.findViewById(R.id.transparent_view);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "received in frag");

        if (resultCode == Activity.RESULT_OK) {
            //String filePath = "";
            if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String tempPath = getPath(selectedImageUri, getActivity());

                openPreview(tempPath);
            }
        }
    }

    @OnClick(R.id.gallery_btn)
    public void fromGalleryClick(View v){

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }

    @OnClick(R.id.snap_btn)
    public void onSnapClick(View v) {

        progressDialog = ProgressDialog.show(getContext(), getResources().getString(R.string.wait),
                getResources().getString(R.string.pic_processing));
       // camera.autoFocus(this);
        camera.takePicture(this, null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera) {

        Bitmap bm = PictureCreator.rotateBitmap(PictureCreator
                .decodeSampledBitmapFromByteArray(paramArrayOfByte, preview.getWidth(), preview.getHeight()), 90);
        try {
            File pic = new File(SymbolRecognition.DATA_PATH + File.separator + System.currentTimeMillis() + ".jpg");
            FileOutputStream os = new FileOutputStream(pic);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            os = null;
            progressDialog.dismiss();
            openPreview(pic.getAbsolutePath());
        }
        catch (Exception e) {
        }

        // после того, как снимок сделан, показ превью отключается. необходимо включить его
        paramCamera.startPreview();
    }

    private void openPreview(String absPath) {

        Bundle bundle = new Bundle();

        // bundle.putSerializable("photoset", p);
        bundle.putString("file", absPath);

        PictureCutFragment pictureCutFragment = PictureCutFragment.newInstance(absPath);

        FragmentUtils.openFragment(pictureCutFragment, R.id.root_frame, "CUT", getActivity(), true);
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onResume() {
        super.onResume();
        camera = Camera.open();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        if (holder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            Camera.Parameters parameters = camera.getParameters();

            // Set the auto-focus mode to "continuous"
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

            camera.setParameters(parameters);
            camera.startPreview();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        Size previewSize = camera.getParameters().getPreviewSize();
        float aspect = (float) previewSize.width / previewSize.height;

        int previewSurfaceWidth = preview.getWidth();
        int previewSurfaceHeight = preview.getHeight();

        LayoutParams lp = preview.getLayoutParams();

        // здесь корректируем размер отображаемого preview, чтобы не было искажений

        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            // портретный вид
            camera.setDisplayOrientation(90);
            lp.height = previewSurfaceHeight;
            lp.width = (int) (previewSurfaceHeight / aspect);

        }
        else {
            // ландшафтный
            camera.setDisplayOrientation(0);
            lp.width = previewSurfaceWidth;
            lp.height = (int) (previewSurfaceWidth / aspect);
        }

     //   preview.setLayoutParams(lp);

        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    public void onShutter() {

    }
}