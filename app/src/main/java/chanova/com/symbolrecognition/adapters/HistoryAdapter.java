package chanova.com.symbolrecognition.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.activities.AddNewItemActivity;
import chanova.com.symbolrecognition.databinding.ItemRecognitionBinding;
import chanova.com.symbolrecognition.interfaces.CRUDAdapterInterface;
import chanova.com.symbolrecognition.models.RecognitionItemModel;
import chanova.com.symbolrecognition.utils.DBUtils.HelperFactory;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> implements CRUDAdapterInterface {

    private static final int UPDATE_ITEM_CODE = 3;
    Context context;
    List<BaseObservable> recognitionItemsList;
    LayoutInflater layoutInflater;

    public HistoryAdapter(Context context) {

        this.context = context;
        recognitionItemsList = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemRecognitionBinding binding;

        @Bind(R.id.delete_btn)
        ImageView delete;

        public ViewHolder(ItemRecognitionBinding binding) {
            super(binding.getRoot());

            this.binding = binding;

            ButterKnife.bind(this, binding.getRoot());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.item_recognition, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        RecognitionItemModel recognitionItemModel = (RecognitionItemModel) getItem(position);

        holder.binding.setItem(recognitionItemModel);
        holder.binding.getRoot().setOnClickListener(view -> {

            showItem(recognitionItemModel);
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    HelperFactory.getHelper().getRecognitionItemDAO().delete(recognitionItemModel);

                    removeItem(recognitionItemModel);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return recognitionItemsList.size();
    }

    @Override
    public BaseObservable getItem(int position) {
        return recognitionItemsList.get(position);
    }

    @Override
    public <T extends BaseObservable> void addItem(T item) {

        recognitionItemsList.add(item);
        notifyDataSetChanged();
    }

    @Override
    public <T extends BaseObservable> void removeItem(T item) {

        recognitionItemsList.remove(item);
        notifyDataSetChanged();
    }

    @Override
    public <T extends BaseObservable> void addItems(List<T> items) {

        recognitionItemsList.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {

        recognitionItemsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public <T extends BaseObservable> void updateItem(int pos, T item) {

        recognitionItemsList.set(pos, item);
        notifyDataSetChanged();
    }

    @Override
    public <T extends BaseObservable> void showItem(T item) {

        Intent intent = new Intent(context, AddNewItemActivity.class);

        intent.putExtra("pos", recognitionItemsList.indexOf(item));
        intent.putExtra("item", (RecognitionItemModel) item);

        ((Activity) context).startActivityForResult(intent, UPDATE_ITEM_CODE);
    }

    @Override
    public List<BaseObservable> getItems() {

        return recognitionItemsList;
    }
}
