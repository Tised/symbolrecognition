package chanova.com.symbolrecognition.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import chanova.com.symbolrecognition.models.RecognitionItemModel;


public class RecognitionItemDAO extends BaseDaoImpl<RecognitionItemModel, String> {

    public RecognitionItemDAO(ConnectionSource connectionSource, Class<RecognitionItemModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<RecognitionItemModel> getAllItems() throws SQLException {

        return this.queryForAll();
    }
}
