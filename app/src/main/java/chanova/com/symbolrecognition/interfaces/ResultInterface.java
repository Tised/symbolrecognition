package chanova.com.symbolrecognition.interfaces;


import android.graphics.Bitmap;

public interface ResultInterface {

    void processResultText(String text);

    void setPic(Bitmap result);
}
