package chanova.com.symbolrecognition.interfaces;

import android.databinding.BaseObservable;

import java.util.List;


public interface CRUDAdapterInterface {

    BaseObservable getItem(int position);
    <T extends BaseObservable> void addItem(T item);
    <T extends BaseObservable> void removeItem(T item);
    <T extends BaseObservable> void addItems(List<T> items);
    void clearList();
    <T extends BaseObservable> void updateItem(int pos, T item);
    <T extends BaseObservable> void showItem(T item);
    List<BaseObservable> getItems();
}
