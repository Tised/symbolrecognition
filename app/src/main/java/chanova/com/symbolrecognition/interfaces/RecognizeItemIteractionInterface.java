package chanova.com.symbolrecognition.interfaces;

import chanova.com.symbolrecognition.models.RecognitionItemModel;

public interface RecognizeItemIteractionInterface {

    void setPicPath(String path);
    void setRecognizedText(String text);
    void setName(String name);
    void setMakeDate(String makeDate);
    RecognitionItemModel getResultModel();
}
