package chanova.com.symbolrecognition.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import chanova.com.symbolrecognition.BR;


@DatabaseTable
public class RecognitionItemModel extends BaseObservable implements Parcelable {

    @DatabaseField(generatedId = true, columnName = "id")
    int id;
    @DatabaseField
    @Bindable
    String name;
    @DatabaseField
    @Bindable
    String picPath;
    @DatabaseField
    @Bindable
    String makeDate;
    @DatabaseField
    @Bindable
    String text;

    public RecognitionItemModel(){}

    protected RecognitionItemModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        picPath = in.readString();
        makeDate = in.readString();
        text = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(picPath);
        dest.writeString(makeDate);
        dest.writeString(text);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RecognitionItemModel> CREATOR = new Creator<RecognitionItemModel>() {
        @Override
        public RecognitionItemModel createFromParcel(Parcel in) {
            return new RecognitionItemModel(in);
        }

        @Override
        public RecognitionItemModel[] newArray(int size) {
            return new RecognitionItemModel[size];
        }
    };

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        notifyPropertyChanged(BR.text);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
        notifyPropertyChanged(BR.picPath);
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
        notifyPropertyChanged(BR.makeDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RecognitionItemModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", picPath='" + picPath + '\'' +
                ", makeDate='" + makeDate + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
