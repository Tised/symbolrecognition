package chanova.com.symbolrecognition.asynctasks;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;

import chanova.com.symbolrecognition.R;
import chanova.com.symbolrecognition.SymbolRecognition;
import chanova.com.symbolrecognition.interfaces.ResultInterface;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class DetectTextTask extends AsyncTask<Bitmap, Void, String> {

    private static final String TAG = "RECOGNIZE_ASYNC";
    ResultInterface resultInterface;
    SweetAlertDialog progress;
    Context context;
    String cropPicPath;
    Bitmap result;

    public DetectTextTask(ResultInterface resultInterface, Context context, String cropPicPath) {

        this.resultInterface = resultInterface;
        this.context = context;
        this.cropPicPath = cropPicPath;
        progress = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progress.setTitleText(context.getString(R.string.analyzing_picture));
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Bitmap... params) {

        ((Activity) context).runOnUiThread(() -> progress.show());

        Mat img = Imgcodecs.imread(cropPicPath);

        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);

        Imgproc.GaussianBlur(img, img, new Size(3, 3), 0);

        Imgproc.threshold(img, img, 0, 255, Imgproc.THRESH_OTSU);

        Imgproc.morphologyEx(img, img, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
        Imgproc.morphologyEx(img, img, Imgproc.MORPH_OPEN, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));

        Imgproc.dilate(img, img,Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2,2)));
//        Imgproc.erode(img, img,Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2,2)));
//        Imgproc.medianBlur(img, img, 15);


        Bitmap toAnalyze = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
        result = toAnalyze;
        Utils.matToBitmap(img, toAnalyze);

        String tessDataPath = SymbolRecognition.DATA_PATH + "/";
        String recognizedText = "";

        try {

            TessBaseAPI tessBaseAPI = new TessBaseAPI();
            tessBaseAPI.init(tessDataPath, "blogger+archivo+lato+lato_1"); //Init the Tess with the trained data file, with english language
            tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "0123456789");
            tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
            tessBaseAPI.setImage(toAnalyze);

            recognizedText = tessBaseAPI.getUTF8Text();

            Canvas canvas = new Canvas(toAnalyze);
            Paint p = new Paint();
            p.setColor(Color.RED);
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeWidth(4);

            for (Rect r : tessBaseAPI.getWords().getBoxRects()) {

                Log.d(TAG, "r = " + r);
                canvas.drawRect(r, p);
            }

            File file = new File(cropPicPath);
            FileOutputStream fOut = new FileOutputStream(file);

            toAnalyze.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            Log.d(TAG, "Got data: " + recognizedText);
            tessBaseAPI.end();

        } catch (Throwable t) {

            Log.e(TAG, "error = " + t.getMessage());
        }

        return recognizedText;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        ((Activity) context).runOnUiThread(() -> progress.hide());
        resultInterface.processResultText(s);

        resultInterface.setPic(BitmapFactory.decodeFile(cropPicPath));
    }
}
